#pragma once

#include <memory>

#include "samovar/incremental/engine/Network.hpp"
#include "samovar/incremental/engine/ExecutorCommand.hpp"

namespace samovar {
namespace incremental {
namespace engine {

class Executor final
{

public:
    Executor(
        std::shared_ptr< Network > network
    );

    void
    evaluate(ExecutorCommand command);

private:
    std::shared_ptr< Network > m_network;

};

}    
}    
}