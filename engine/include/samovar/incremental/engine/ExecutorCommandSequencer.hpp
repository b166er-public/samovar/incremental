#pragma once

#include <memory>
#include <vector>

#include "samovar/incremental/engine/Network.hpp"
#include "samovar/incremental/engine/ExecutorCommand.hpp"

namespace samovar {
namespace incremental {
namespace engine {

class ExecutorCommandSequencer final
{

public:
    ExecutorCommandSequencer(
        std::shared_ptr< Network > network
    );

    std::vector<ExecutorCommand> 
    calculate_sequence(void);

private:
    std::shared_ptr< Network > m_network;

};

}    
}    
}