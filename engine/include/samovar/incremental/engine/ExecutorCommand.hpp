#pragma once

namespace samovar {
namespace incremental {
namespace engine {

enum ExecutorOperation {
    COMPONENT_INIT,
    COMPONENT_RENDER_NEW_STATE,
    COMPONENT_RENDER_OUTPUT_LAYER,
    WIRE_EVALUATE
};

class ExecutorCommand final
{

public:
    ExecutorCommand(
        ExecutorOperation operation,
        unsigned int target_id
    );

    ExecutorOperation
    get_operation(void);

    unsigned int
    get_target_id(void);

private:
    ExecutorOperation m_operation;
    unsigned int m_target_id;

};

}    
}    
}