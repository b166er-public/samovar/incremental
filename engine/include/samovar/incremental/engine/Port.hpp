#pragma once

namespace samovar {
namespace incremental {
namespace engine {

class Port final
{

public:
    Port(float default_value=0.0f);

    void 
    setValue(float new_value);
    
    float 
    getValue(void);

private:
    float m_value;

};

}    
}    
}