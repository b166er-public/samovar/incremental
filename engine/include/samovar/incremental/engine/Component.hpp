#pragma once

#include <map>
#include <vector>
#include <string>
#include <optional>

#include "samovar/incremental/engine/Port.hpp"

namespace samovar {
namespace incremental {
namespace engine {

using ParameterMap = std::map<std::string,std::string>;

class Component
{

public:
    Component(ParameterMap parameters);
    virtual ~Component() = default;

    float
    get_output_value(unsigned int port_id);

    void
    set_input_value(unsigned int port_id, float port_value);

    unsigned int
    get_number_of_input_ports(void);

    unsigned int
    get_number_of_output_ports(void);

public:
    virtual void
    init() = 0;

    virtual void
    render_new_state() = 0;

    virtual void
    render_output_layer() = 0;

    virtual bool
    is_direct_forwarding() = 0;

protected:
    unsigned int
    add_input_port(float default_value = 0.0f);

    unsigned int
    add_output_port(float default_value = 0.0f);

    std::optional< std::string >
    get_parameter(std::string key);

private:
    ParameterMap        m_parameters;
    std::vector<Port>   m_input_ports;
    std::vector<Port>   m_output_ports;

};

}    
}    
}