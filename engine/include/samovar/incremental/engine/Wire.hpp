#pragma once

namespace samovar {
namespace incremental {
namespace engine {

using ComponentId = unsigned int;
using PortId = unsigned int;

class Wire final
{

public:
    Wire(
        ComponentId source_component_id,
        PortId      source_port_id,
        ComponentId target_component_id,
        PortId      target_port_id
    );

    ComponentId 
    get_source_component_id();

    PortId 
    get_source_port_id();

    ComponentId 
    get_target_component_id();

    PortId 
    get_target_port_id();

private:
    ComponentId     m_source_component_id;
    PortId          m_source_port_id;
    ComponentId     m_target_component_id;
    PortId          m_target_port_id;

};

}    
}    
}