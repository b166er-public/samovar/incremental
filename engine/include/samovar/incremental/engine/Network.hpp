#pragma once

#include <memory>
#include <optional>
#include <set>

#include "samovar/incremental/engine/Component.hpp"
#include "samovar/incremental/engine/Wire.hpp"

namespace samovar {
namespace incremental {
namespace engine {


class Network final
{

public:
    Network() = default;
    ~Network() = default;

    unsigned int 
    add_component(
        std::shared_ptr<Component> component_prt
    );
    
    unsigned int 
    add_wire(
        std::shared_ptr<Wire> wire_prt
    );

    std::optional< std::shared_ptr< Component > >
    get_component(unsigned int index);

    std::optional< std::shared_ptr< Wire > >
    get_wire(unsigned int index);

    unsigned int
    get_number_of_components(void);

    unsigned int
    get_number_of_wires(void);

    std::set<unsigned int>
    calculate_component_incomming_wire_set(unsigned int component_index);

    std::set<unsigned int>
    calculate_component_outgoing_wire_set(unsigned int component_index);

    bool
    is_component_incomming_layer_complete(unsigned int component_index);

    bool
    is_component_outgoing_layer_complete(unsigned int component_index);

private:
    std::vector< std::shared_ptr<Component> > m_components;
    std::vector< std::shared_ptr<Wire> > m_wires;

};

}    
}    
}