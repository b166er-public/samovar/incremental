#include "samovar/incremental/engine/Port.hpp"

namespace samovar {
namespace incremental {
namespace engine {

Port::Port(float default_value) : m_value{default_value}
{ }

void
Port::setValue(float new_value)
{
    this->m_value = new_value;
}

float
Port::getValue()
{
    return this->m_value;
}



}    
}    
}