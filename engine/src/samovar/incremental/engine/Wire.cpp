#include "samovar/incremental/engine/Wire.hpp"

namespace samovar {
namespace incremental {
namespace engine {


Wire::Wire(
    ComponentId source_component_id,
    PortId      source_port_id,
    ComponentId target_component_id,
    PortId      target_port_id
) : m_source_component_id{source_component_id},
    m_source_port_id{source_port_id},
    m_target_component_id{target_component_id},
    m_target_port_id{target_port_id}
{ }

ComponentId 
Wire::get_source_component_id() 
{
    return this->m_source_component_id;
}

PortId 
Wire::get_source_port_id()
{
    return this->m_source_port_id;
}

ComponentId 
Wire::get_target_component_id()
{
    return this->m_target_component_id;
}

PortId 
Wire::get_target_port_id()
{
    return this->m_target_port_id;
}


}    
}    
}