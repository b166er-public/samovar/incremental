#include "samovar/incremental/engine/ExecutorCommand.hpp"

namespace samovar {
namespace incremental {
namespace engine {

ExecutorCommand::ExecutorCommand(
    ExecutorOperation operation,
    unsigned int target_id
) : m_operation{operation}, m_target_id{target_id}
{

}

ExecutorOperation
ExecutorCommand::get_operation(void)
{
    return this->m_operation;
}

unsigned int
ExecutorCommand::get_target_id(void)
{
    return this->m_target_id;
}



}    
}    
}