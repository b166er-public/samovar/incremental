#include "samovar/incremental/engine/Executor.hpp"

namespace samovar {
namespace incremental {
namespace engine {


Executor::Executor(
    std::shared_ptr< Network > network
) : m_network{network}
{

}

void
Executor::evaluate(
    ExecutorCommand command
)
{
    switch(command.get_operation())
    {
        case COMPONENT_INIT:
        {
            auto c = this->m_network->get_component( 
                command.get_target_id() 
            );
            if(c.has_value()) { c.value()->init(); }
            break;
        }

        case COMPONENT_RENDER_NEW_STATE:
        {
            auto c = this->m_network->get_component(
                command.get_target_id()
            );
            if(c.has_value()) { c.value()->render_new_state(); }
            break;
        }

        case COMPONENT_RENDER_OUTPUT_LAYER:
        {
            auto c = this->m_network->get_component(
                command.get_target_id()
            );
            if(c.has_value()) { c.value()->render_output_layer(); }
            break;
        }

        case WIRE_EVALUATE:
        {
            auto w_optional = this->m_network->get_wire(
                command.get_target_id() 
            );
            if(w_optional.has_value())
            {
                auto w = w_optional.value();

                auto src_component = this->m_network->get_component(
                    w->get_source_component_id()
                );

                auto trg_component = this->m_network->get_component(
                    w->get_source_component_id()
                );

                if(src_component.has_value() && trg_component.has_value())
                {
                    float value = src_component.value()->get_output_value(
                        w->get_source_port_id()
                    );

                    trg_component.value()->set_input_value(
                        w->get_target_port_id(),
                        value
                    );
                }

            }
            break;
        }
    }
}



}    
}    
}