#include "samovar/incremental/engine/Network.hpp"

#include <algorithm>

namespace samovar {
namespace incremental {
namespace engine {



unsigned int 
Network::add_component(
    std::shared_ptr<Component> component_prt
)
{
    auto new_id = this->m_components.size();
    this->m_components.push_back(component_prt);
    return new_id;
}

unsigned int 
Network::add_wire(
    std::shared_ptr<Wire> wire_prt
)
{
    auto new_id = this->m_wires.size();
    this->m_wires.push_back(wire_prt);
    return new_id;
}

std::optional< std::shared_ptr< Component > >
Network::get_component(unsigned int index)
{
    if(index < this->m_components.size() )
    {
        return this->m_components[index];
    }
    else
    {
        return std::nullopt;
    }
}

std::optional< std::shared_ptr< Wire > >
Network::get_wire(unsigned int index)
{
    if(index < this->m_wires.size() )
    {
        return this->m_wires[index];
    }
    else
    {
        return std::nullopt;
    }
}

unsigned int
Network::get_number_of_components(void)
{
    return this->m_components.size();
}

unsigned int
Network::get_number_of_wires(void)
{
    return this->m_wires.size();
}

std::set<unsigned int>
Network::calculate_component_incomming_wire_set(unsigned int component_index)
{
    std::set<unsigned int> result;

    if(component_index < this->m_components.size())
    {
        auto number_of_wires = get_number_of_wires();
        for(unsigned int w_idx = 0; w_idx < number_of_wires; w_idx++)
        {
            auto w = get_wire(w_idx).value();
            if(w->get_target_component_id() == component_index)
            {
                result.insert(w_idx);
            }
        }
    }

    return result;
}

std::set<unsigned int>
Network::calculate_component_outgoing_wire_set(unsigned int component_index)
{
    std::set<unsigned int> result;

    if(component_index < this->m_components.size())
    {
        auto number_of_wires = get_number_of_wires();
        for(unsigned int w_idx = 0; w_idx < number_of_wires; w_idx++)
        {
            auto w = get_wire(w_idx).value();
            if(w->get_source_component_id() == component_index)
            {
                result.insert(w_idx);
            }
        }
    }

    return result;
}

bool
Network::is_component_incomming_layer_complete(unsigned int component_index)
{
    auto incomming_wire_set = 
        calculate_component_incomming_wire_set(component_index);

    std::set<unsigned int> connected_ports;
    std::for_each(
        incomming_wire_set.begin(),
        incomming_wire_set.end(),
        [&](unsigned int const& w_idx) { 
            connected_ports.insert(
                this->get_wire(w_idx).value()->get_target_port_id()
            );
         }
    );

    unsigned int number_of_input_ports =
        get_component(component_index).value()->get_number_of_input_ports();
    bool is_complete = true;
    for(unsigned int p_idx = 0; p_idx < number_of_input_ports; p_idx++)
    {
        is_complete &= (connected_ports.count(p_idx) > 0);
    }

    return is_complete;
}

bool
Network::is_component_outgoing_layer_complete(unsigned int component_index)
{
    auto outgoing_wire_set = 
        calculate_component_outgoing_wire_set(component_index);

    std::set<unsigned int> connected_ports;
    std::for_each(
        outgoing_wire_set.begin(),
        outgoing_wire_set.end(),
        [&](unsigned int const& w_idx) { 
            connected_ports.insert(
                this->get_wire(w_idx).value()->get_source_port_id()
            );
         }
    );

    unsigned int number_of_output_ports =
        get_component(component_index).value()->get_number_of_output_ports();
    bool is_complete = true;
    for(unsigned int p_idx = 0; p_idx < number_of_output_ports; p_idx++)
    {
        is_complete &= (connected_ports.count(p_idx) > 0);
    }

    return is_complete;
}

}    
}    
}