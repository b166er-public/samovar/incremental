#include "samovar/incremental/engine/Component.hpp"

namespace samovar {
namespace incremental {
namespace engine {

Component::Component(ParameterMap parameters)
: m_parameters{parameters}
{ }

float
Component::get_output_value(unsigned int port_id)
{
    if(port_id < this->m_output_ports.size())
    {
        return this->m_output_ports[port_id].getValue();
    }
    else
    {
        return 0.0;
    }
}

void
Component::set_input_value(unsigned int port_id, float port_value)
{
    if(port_id < this->m_input_ports.size())
    {
        this->m_output_ports[port_id].setValue(port_value);
    }
}

unsigned int
Component::get_number_of_input_ports(void)
{
    return this->m_input_ports.size();
}

unsigned int
Component::get_number_of_output_ports(void)
{
    return this->m_output_ports.size();
}

unsigned int
Component::add_input_port(float default_value)
{
    auto new_id = this->m_input_ports.size();

    this->m_input_ports.push_back(
        Port(default_value)
    );

    return new_id;
}

unsigned int
Component::add_output_port(float default_value)
{
    auto new_id = this->m_output_ports.size();

    this->m_output_ports.push_back(
        Port(default_value)
    );

    return new_id;
}

std::optional< std::string >
Component::get_parameter(std::string key)
{
    if(this->m_parameters.count(key) > 0)
    {
        return this->m_parameters[key];
    }
    else
    {
        return std::nullopt;
    }
}


}    
}    
}