#include "samovar/incremental/engine/ExecutorCommandSequencer.hpp"

#include <set>
#include <algorithm>
#include <stdexcept>

namespace samovar {
namespace incremental {
namespace engine {

ExecutorCommandSequencer::ExecutorCommandSequencer(
    std::shared_ptr< Network > network
) : m_network{network}
{ }

std::vector<ExecutorCommand> 
ExecutorCommandSequencer::calculate_sequence(void)
{
    std::vector<ExecutorCommand> result;

    auto number_of_components = m_network->get_number_of_components();

    std::set<unsigned int> tecs; // to evaluate component set

    std::set<unsigned int> evaluated_wires;

    // Lambda to set a new component being evaluated
    auto mark_component_as_evaluated = [&](unsigned int component_idx) { 
        // Remove node from unevaluated list
        tecs.erase(component_idx);

        // Add outgoing wires to the evaluated wires set
        auto outgoing_wires = m_network->calculate_component_outgoing_wire_set(component_idx);
        std::for_each(
            outgoing_wires.begin(),
            outgoing_wires.end(),
            [&](unsigned int const& w_idx) { 
                evaluated_wires.insert(w_idx);
            }
        );
    };

    /**
     * Add all components to the tecs
     */
    for(unsigned int c = 0; c < number_of_components; c++)
    {
        tecs.insert(c);
    }

    /**
     * List all non direct forwarding components 
     * as they can be evaluated immediately
     */
    std::set<unsigned int> direct_forwarding_components;
    for(unsigned int c = 0; c < number_of_components; c++)
    {
        if( !(m_network->get_component(c).value()->is_direct_forwarding()) )
        {
            result.push_back(
                ExecutorCommand(
                    ExecutorOperation::COMPONENT_RENDER_OUTPUT_LAYER,
                    c
                )
            );
            direct_forwarding_components.insert(c);
            mark_component_as_evaluated(c);

        }
    }

    /**
     * Go through the tecs and look for something to evaluate
     */
    bool found_at_least_one_component = true;
    while(found_at_least_one_component)
    {
        found_at_least_one_component = false;

        for(unsigned int c_idx : tecs)
        {
            auto incomming_wire_set = 
                m_network->calculate_component_incomming_wire_set(c_idx);

            bool is_subset = std::includes(
                evaluated_wires.begin(), evaluated_wires.end(),
                incomming_wire_set.begin(), incomming_wire_set.end()
            );

            if(is_subset)
            {
                result.push_back(
                    ExecutorCommand(
                        ExecutorOperation::COMPONENT_RENDER_NEW_STATE,
                        c_idx
                    )
                );
                result.push_back(
                    ExecutorCommand(
                        ExecutorOperation::COMPONENT_RENDER_OUTPUT_LAYER,
                        c_idx
                    )
                );
                mark_component_as_evaluated(c_idx);
                found_at_least_one_component |= true;
            }
        }
    }

    // Check if all components had been evaluted yet
    if(!tecs.empty())
    {
        throw std::runtime_error("Network seems to be incomplete as no complete evaluation is possible!");
    }
    else
    {
        for(unsigned int dfc_idx : direct_forwarding_components)
        {
            result.push_back(
                ExecutorCommand(
                    ExecutorOperation::COMPONENT_RENDER_NEW_STATE,
                    dfc_idx
                )
            );
        }
        
    }

    return result;
}



}    
}    
}