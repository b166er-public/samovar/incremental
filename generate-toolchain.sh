#!/bin/bash

rm -rf build
mkdir build
mkdir build/toolchain
mkdir build/artifacts
cmake -S . -B ./build/toolchain -DCMAKE_INSTALL_PREFIX="${PWD}/build/artifacts"